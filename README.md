**Развернуть front end для приложения**

В качестве front end'a будем использовать Nginx

**Предпосылки**

* Установить git-client
* Установить сервис nginx


**Установка**

>yum install git
>yum install nginx


**Развертывание**


Сконфигурить Nginx, в нашем случае подойдет дефолтный файл nginx.conf
```  user  nginx;
  worker_processes  1;

  error_log  /var/log/nginx/error.log warn;
  pid        /var/run/nginx.pid;


  events {
      worker_connections  1024;
  }


  http {
  include       /etc/nginx/mime.types;
  default_type  application/octet-stream;

  log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                    '$status $body_bytes_sent "$http_referer" '
                    '"$http_user_agent" "$http_x_forwarded_for"';

  access_log  /var/log/nginx/access.log  main;
  sendfile        on;
  #tcp_nopush     on;

  keepalive_timeout  65;

  #gzip  on;

  include /etc/nginx/conf.d/*.conf;
  }```
 